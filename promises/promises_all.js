const { searchUserByIdPromise } = require('./data/user')
const { searchBooksByUserNamePromise } = require('./data/books')

const searchUserPromise = searchUserByIdPromise(1)
const searchBookPromise = searchBooksByUserNamePromise('Reinaldo')

Promise.all([
    searchUserPromise,
    searchBookPromise
]).then(values => console.log(values)).catch(error => console.log(error))