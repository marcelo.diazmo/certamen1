exports.searchUserByIdPromise = (id) => {
    const promise = new Promise((resolve, reject) => {
        setTimeout(() => {
            resolve({ id, name: 'Francisco Cabezas' })
        }, 200)
    })
    return promise
}

exports.searchUserByIdCallback = (id, callback) => {
    setTimeout(() => {
        callback({ id, name: 'Francisco Cabezas' })
    }, 200)
}