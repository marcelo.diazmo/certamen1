const express = require('express')
const { getUser, getUsers } = require('./src/userRepository')
const { createUser, validateUser } = require('./src/responseManager')

const app = express()

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))

app.get('/', function (request, response) {
    response.send('HOLA MUNDO por GET')
})

app.get('/api/customer', function (request, response) {
    response.status(200).json(getUsers())
})

app.get('/api/customer/:id', function (request, response) {
    const user = getUser(request.params.id)
    validateUser(user, response)
})

app.post('/api/customer', function (request, response) {
    createUser(request.body.name, request.body.profession, response)
})

app.listen(3000, function () {
    console.log('Servidor corriendo en el puerto 3000')
})