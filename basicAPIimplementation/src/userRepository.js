const user1 = {
    id: 12345,
    name: 'Francisco Cabezas',
    profession: 'Ingeniero Civil Telemático'
}

const user2 = {
    id: 98765,
    name: 'Javiera',
    profession: 'Constructora'
}

const users = [
    user1,
    user2
]

exports.getUser = (id) => {
    return users.filter(data => data.id == id)
}

exports.getUsers = () => {
    return users
}

exports.createUser = (name, profession) => {
    const user = {
        id: 123456,
        name,
        profession
    }
    return user
}