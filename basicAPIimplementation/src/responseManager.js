const { createUser } = require('./userRepository')

exports.validateUser = (user, response) => {
    if (user.length > 0) {
        response.status(200).json(user)
    } else {
        response.status(400).json({
            message: 'User not found'
        })
    }
}

exports.createUser = (name, profession, response) => {
    if (name === undefined || profession === undefined) {
        response.status(500).json({
            error: true,
            message: 'Uno de los parámetros no llegó correctamente'
        })
    } else {
        response.status(200).json(createUser(name, profession))
    }
}