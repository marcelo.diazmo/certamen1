const mathFuncions = require('./mathFunctions')
const iterators = require('./iterators')
const objects = require('./objects')

// var --> permite que las variables sean globales y mutables
// let --> declara variables de scope (mutable)
// const --> variable constante (inmutable)
const numero = '50'
const palabra = 'Cincuenta'

const suma = palabra + numero

const castNumero = Number(numero) + 100

console.log(suma)
console.log(castNumero)

iterators.printStringArrayUsingFor(palabra)

console.log('----------------------')

iterators.printStringArrayUsingWhile(palabra)

console.log('----------------------')

console.log(mathFuncions.sum(500, 50))

const person1 = {
    id: 123456,
    name: 'Francisco Cabezas',
    profession: 'Ingeniero Civil Telemático'
}

const person2 = {
    id: 987674,
    name: 'Ricardo Águila',
    profession: 'Médico'
}

console.log(person1.id)
console.log(person1.name)
console.log(person1['profession'])

const personsArray1 = [
    person1,
    person2
]

const personsArray2 = []
personsArray2.push(person1)
personsArray2.push(person2)

const stringArray = ['Hola', 'Mundo', 'cómo están']
const numberArray = [1, 2, 3, 4, 5]

objects.printArray(personsArray1)
objects.printArray(stringArray)
objects.printArray(numberArray)