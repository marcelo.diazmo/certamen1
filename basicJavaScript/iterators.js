exports.printStringArrayUsingFor = (input) => {
    for(let i = 0; i < input.length; i++) {
        console.log(input[i])
    }
}

exports.printStringArrayUsingWhile = (input) => {
    let i = 0
    while (i < input.length) {
        console.log(input[i])
        i++
    }
}